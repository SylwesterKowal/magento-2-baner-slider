var config = {
    paths: {
        'jquery_bannerslider': 'Kowal_Banner/js/jquery.bannerslider'
    },
    shim: {
        'jquery_bannerslider': {
            'deps': ['jquery']
        }
    }
};